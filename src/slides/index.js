
let slides = [];

import logo from 'images/logo-asc.png';

let slide_title = `
<section id="cover" data-background-image="images/header_02.jpg">
	<img src="${logo}" />
	<h3 style="color: white;">Test Performance</h3>
    <p style="color: white;">Introducción y Principios</p>
</section>
`;

slides.push(slide_title);

let slide_agenda = `
<section data-markdown>
	<textarea data-template>
## Agenda

- Porque los test de performance son importantes?
- Porque Taurus?
- Introducción a Jmeter
- Taurus + Jmeter
- Ejemplo de un reporte de test
	</textarea>
</section>
`;
slides.push(slide_agenda);

let slide_intro = `
<section>
<h3>Porque son importantes?</h3>
	<section>
	<img src="images/performancetesting.jpg"/>
	</section>
	<section>
	- Permite identificar los cuellos de botella de la aplicación
	</section>
	<section>
	- Descubrir los casos en donde la concurrencia de usuario falla
	</section>
	<section>
	- Saber el comportamiento de la aplicación en la carga de datos
	</section>
	<section>
	- Conocer la cantidad de usuario que puede manejar el sistema
	</section>
	<section>
	- Permite integrarlo con jenkins
	</section>
	<section>
	- etc...
	</section>
</section>
`;
slides.push(slide_intro);

let get_taurus = `
<section data-markdown>
	<textarea data-template>
### Porque [Taurus](https://gettaurus.org/)?
		
<section data-markdown>

- facil de instalar

- es una capa de abstracción de: [JMeter](https://jmeter.apache.org/), [Locust](https://locust.io/), [Gatling](https://gatling.io/), [Grinder](http://grinder.sourceforge.net/) y [Selenium](https://www.seleniumhq.org/).

- permite parametrizar test
</section>

<section>

#### Instalación

\`\`\`bash
pip install bzt 
\`\`\`

</section>
<section>

#### Test

\`\`\`yaml
execution:
- concurrency: 100
  ramp-up: 1m
  hold-for: 5m
  scenario: quick-test

scenarios:
  quick-test:
    requests:
    - http://blazedemo.com
\`\`\`

</section>
<section>

#### Ejecución

\`\`\`bash
bzt quick_test.yml
\`\`\`

</section>

<section>
	<img src="images/console.png"/>
</section>

	</textarea>
</section>
`;

slides.push(get_taurus)

let jmeter = `
<section data-markdown>
	<textarea data-template>
### Introducción a [Jmeter](https://jmeter.apache.org/)?
		
<section data-markdown>

- Independeite de la plataforma
- open source
- permite editar test facilmente

</section>

<section>

#### Instalación

\`\`\`bash
taurus lo hace!
\`\`\`

</section>
<section>

#### Test

[Demo]

</section>
<section>

#### Ejecución

[Demo]

</section>

	</textarea>
</section>
`;

slides.push(jmeter)

let note = `
<section data-markdown>
	<textarea data-template>

## Taurus + Jmeter

<section>

\`\`\`yaml
execution:
- iterations: 50
  concurrency: 10
  scenario: with_script

scenarios:
  with_script:
    script: my-existing.jmx
    
reporting:
- module: passfail
  criteria:
  - "avg-rt>150ms for 10s, continue as failed"
  - "fail>50% for 10s, stop as failed"
\`\`\`

</section>

<section>

[Demo]

</section>

</textarea>
</section>
`
slides.push(note);


let preguntas = `
<section data-markdown>
	<textarea data-template>
## Preguntas?

<img src="images/preguntas.jpg"/>
	
</textarea>
</section>
`
slides.push(preguntas);

export default slides;